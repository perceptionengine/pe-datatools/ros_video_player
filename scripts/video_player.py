#!/usr/bin/env python
# Perception Engine. All rights reserved 2022.

from __future__ import print_function

import roslib

roslib.load_manifest('video_player')

import rospy
import numpy as np
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import itertools
import ast


class VideoPlayer:
    def __init__(self):
        rospy.init_node("video_player", anonymous=True)

        self.__videos = rospy.get_param('~videos', ['/home/ne0/Downloads/pe_video.mp4'])
        try:
            self.__videos = ast.literal_eval(self.__videos)
        except:
            rospy.logerr("Invalid 'input_topics' param")
        print(self.__videos)

        self.__topic_iterator = itertools.cycle(self.__videos)
        self.__bridge = CvBridge()

        self.__fps = rospy.get_param('~fps', 40)
        ns = 1000000000 / self.__fps
        duration = rospy.Duration(0, ns)
        rospy.loginfo("Publishing to: {} [sensor_msgs/Image]".format("image_video"))
        self.__video_pub = rospy.Publisher("image_video", Image, queue_size=1)

        while not rospy.is_shutdown():
            current_file = next(self.__topic_iterator)
            rospy.loginfo("Opening file to: {}".format(current_file))
            if current_file == "":
                rospy.logerr("Invalid file: '{}'".format(current_file))
                continue
            cap = cv2.VideoCapture(current_file)
            # Check if video opened successfully
            if not cap.isOpened():
                rospy.logerr("Error opening video stream or file")
                continue
            # Read until video is completed
            while cap.isOpened() and not rospy.is_shutdown():
                if self.__video_pub.get_num_connections() <= 0:
                    rospy.loginfo_throttle(1, "No subscribers. Not playing back")
                else:
                    # Read frame-by-frame
                    ret, frame = cap.read()
                    if ret:
                        try:
                            ros_image = self.__bridge.cv2_to_imgmsg(frame, "bgr8")
                            ros_image.header.frame_id = "video"
                            ros_image.header.stamp = rospy.Time.now()
                            self.__video_pub.publish(ros_image)
                        except CvBridgeError as e:
                            print(e)
                    else:
                        rospy.loginfo("Finished playback:{}".format(current_file))
                        break
                rospy.sleep(duration)

            # When everything done, release the video capture object
            cap.release()


if __name__ == "__main__":
    node = VideoPlayer()
